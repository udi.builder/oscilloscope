#ifndef ILI9320_H_
#define ILI9320_H_
#include "common.h"

//! перечисление используемых растровых шрифтов f16 - 12x16, f10 - 7x10, f8 - 6x8.
enum{f8=0,f10,f16};

//! вывод на дисплей символа
uint16_t Symbol(uint16_t x, uint8_t y, uint8_t d, uint16_t color, uint16_t front, uint8_t font);
//! инициализация контроллера дисплея.
void initLCD(void);
//! заполнение дисплея одним цветом
void Fill(uint16_t color);
//! вертикальная сетка осцилограммы
void Grid_v(uint16_t color);
//! горизонтальная сетка осцилограммы
void Grid_h(uint16_t color);
//! пиксель
void Pixel(uint_fast16_t x, uint_fast16_t y, uint_fast16_t color);
//! стирание предыдущей горизонтальной линии осцилограммы
void clearLine(uint_fast16_t x, uint_fast8_t y1, uint_fast8_t y2, uint_fast16_t color, uint16_t front);
//! рисование новой горизонтальной линии осцилограммы
void setLine(uint_fast16_t x, uint_fast8_t y1, uint_fast8_t y2, uint_fast16_t color);
//! горизонтальная линия
void Line_h(uint16_t x, uint16_t y, uint16_t count, uint16_t color);
//! вертикальная линия
void Line_v(uint16_t x, uint16_t y, uint8_t count, uint16_t color);
//! заполненый цветом прямоугольник
void Rectangle(uint16_t x, uint16_t y, uint16_t count_x, uint16_t count_y, uint16_t color);
//! десятичное 3-х разрядное число
uint16_t Digital3(uint16_t x,uint16_t y,uint16_t d,uint16_t c, uint16_t front, uint8_t font);
//! десятичное 5 разрядное число
uint16_t Digital5(uint16_t x,uint16_t y,uint16_t d,uint16_t c, uint16_t front, uint8_t font);
//! десятичное число с десятичной точкой и гашением незначащих нулей
uint16_t Digital(uint16_t x,uint16_t y,uint16_t d, uint8_t decimal, uint16_t c, uint16_t front, uint8_t font);
//! строка
uint16_t Message(uint16_t x,uint16_t y, const char *adr, uint16_t color, uint16_t front, uint8_t font);
//! Рамка с двойной окантовкой заполненная цветом.
void setButton(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color, uint16_t back);
void drawHex8(uint8_t x,uint8_t y,uint32_t c,uint16_t color, uint16_t front, uint8_t font);
void drawHex4(uint8_t x,uint8_t y,uint32_t c,uint16_t color, uint16_t front, uint8_t font);
#endif /* I9320_H_ */
