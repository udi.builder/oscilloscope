/*! \mainpage Простой осцилограф.
 *
 * \author    Daniil Ulanov
 * \author    Igor Ulanov
 * \version   1.0
 * \date      2014
 *
 * \section intro_sec Описание.
 *
 * \subsection task Задача.
 *
 * Измерять сигнал на входе АЦП и выводить его на дисплей в виде осцилограммы.\n
 * Размер окна для вывода 250х200 px. Сетка 10х8 делений.\n
 * Положения вертикальной развертки:
 * - 12.5 mV/деление
 * - 25 mV/деление
 * - 50 mV/деление
 * - 0.125 V/деление
 * - 0.25 V/деление
 * - 0.5 V/деление
 *
 * Положения горизонтальной развертки:
 * - 0.1 mS/деление
 * - 0.2 mS/деление
 * - 0.5 mS/деление
 * - 1 mS/деление
 * - 2 mS/деление
 * - 5 mS/деление
 *
 * \subsection r_sect Реализация.
 *
 * Осцилограф реализован на базе модуля LCD-STM32 фирмы Olimex. Модуль построен на базе ARM контроллера STM32F103
 * с 4.3 дюймовым ЖК дисплеем и сенсорным экраном. Для измерения входного сигнала используется АЦП1 микроконтроллера.
 * Сигнал поступает через согласующий повторитель на AD820.
 *
 * \subsection instr Инструментарий.
 *
 * Разработка программного обеспечения велась с помощью IDE Eclipse IDE for C/C++ Developers.
 * Version: Indigo Service Release 2. Build id: 20120216-1857. Для компиляции использовался
 * тулчайн Sourcery CodeBench Lite 2011.09-69.\n
 * Для загрузки программного обеспечения в микроконтроллер модуля можно воспользоваться программатором ST-LINK.
 * Файлы программного обеспечения project.bin и project.hex находится в папке out\n
 *
 * \section algoritm Алгоритм.
 *
 * \subsection k_secr Кратко.
 *
 * Программа выполняет следующие задачи:
 * - измерение и обработка входного сигнала.
 * - измерение и обработка резисторов сенсорного экрана
 * - вывод информации на ЖК дисплей.
 *
 * Для измерения сигнала и сенсорного экрана используются АЦП1 и АЦП3 соотвественно.\n
 * АЦП1 работает в режиме однократного запуска. АЦП1 каждые 4 мкс запускает таймер4. С помощью ПДП1 измеренные значения
 * сохраняются в массиве размером 2500 слов(10мс). В прерывании #DMA1_Channel1_IRQHandler от ПДП1 осуществляется
 * пересчет значений в координату Х дисплея. Вывод на экран этих значений осуществляется в функции #main.\n
 * АЦП3 работает в беспрерывном циклическом режиме. Результаты измерения сохраняются в памяти используя механизм ПДП.
 * По окончании цикла ПДП генерируется прерывание #DMA2_Channel4_5_IRQHandler, в котором осуществляется предварительная
 * обработка оцифрованных сигналов. Пересчет значений в координаты дисплея осуществляется в прерывании #EXTI3_IRQHandler.
 *
 * \subsection p_sect Подробно.
 *
 * \subsubsection back_sect Измерение и обработка входного сигнала.
 *
 * Сигнал измеряется каналом 8 АЦП1. В функции прерывания #DMA1_Channel1_IRQHandler производится разбор
 * сохраненых в массиве #measure результатов измерения. Этот масcив наполняется с помощью цикла ПДП, который сохраняет
 * #MEASURING_COUNT измерений. В цикле этой функции выбираются необходимые значения для вывода на дисплей, в зависимости
 * от установленного делителя горизонтальной развертки #storage_k. В соответсвии со значением этого делителя выбирается
 * коэффициент суммирования значений массива #measure. Коэффициенты хранятся в массиве структур #storage. Из выбранных
 * значений высчитывается среднеарифмитическое. Это среднеарифмитическое преобразуется в координаты оси Y дисплея
 * умножением на коэффициент умножения, который выбирается из массива структур #amplitude в зависимости от установленного
 * делителя вертикальной развертки #amplitude_k.
 *
 * \subsubsection touch Сенсорный экран.
 *
 * Для измерения резистивной матрицы сенсорного экрана используются каналы 10, 11, 12, 13 АЦП 3. Результаты измерения
 * обрабатываются в функции #DMA2_Channel4_5_IRQHandler. В этой функции производится переключение каналов для опроса
 * матрицы и сохранения результатов обмера матрицы в массивах #ts_adc_y и #ts_adc_x. Здесь же определяется факт нажатия
 * сенсорного экрана.\n
 * Окончательная обработка происходит в прерывании #EXTI3_IRQHandler. Здесь происходит фильтрация значений и перевод этих
 * значений в координаты дисплея. Из этого прерывания вызывается функция #Navigator обработки нажатий сенсорного дисплея.
 * Входным параметром этой функции является код нажатой области сенсорного дисплея или 0xFF при отсуствии нажатия.
 * Этот код возвращает функция #pressTouch. Номера области определяются по структурам #REGION в которых описываются эти области.
 * Ссылка на ту или иную структуру определяется в массиве структур #LINK_REGION. Индексом (номер позиции меню) служит входной
 * параметр при вызове функции #checkTouch.\n
 * При включении осцилоскопа проверяются корректировочные коэффициенты сенсорного экрана, в случае если они равны значениям
 * по умолчанию, будет предложено произвести калибровку дисплея. Калибровка осуществляется функцией #refreshMenuTouch, которая
 * перезапишет значения коэффициентов структуры #ADJUSTMENT.\n
 *
 * \subsubsection disp Вывод информации на ЖК дисплей.
 *
 * Все функции вывода информации на дисплей заключены в файл Ili9320.c.\n
 * Инициализацию дисплея осуществляет функция #initLCD. Вывод символа функция #Symbol.
 * Вывод десятичных чисел функции: #Digital, #Digital5, #Digital3.
 * Сетка области вывода осцилограммы рисуется функциями: #Grid_h и #Grid_v. Сама осцилограмма выводится
 * с помощью функции #Line. Эта функция просто рисует вертикальную линию или ставит точку, если длина линии равна 1 пикселю.\n
 * Координаты этой линии высчитываются в цикле функции #main. Этот цикл запускается если поле adc структуры #flags равна 1,
 * которое устанавливается в прерывании #DMA1_Channel1_IRQHandler каждые 51.2 мс.\n
 * Цикл последовательно выполняет #VIEW_WIDTH итераций, для каждой координаты Х осцилограммы.
 * Сначала стирается предыдущая осцилограмма в данной точке Х, после чего определяются координаты новой линии от координаты Y
 * предыдущей точки Х до координаты Y настоящей. Координаты высчитываются так, чтобы не было соприкосновения с линией осцилограммы
 * предыдущей координаты Х.
 *
 * \subsubsection flash Данные в энергонезависимой памяти (flash).
 *
 * Во флеш хранится структура #ADJUSTMENT с поправочными коэффициентами сенсорного дисплея.\n
 * Для этой струтуры отведена одна страница флеш. В контроллере STM32F103ZET6 страница имеет размер 2 кБ.
 * Новые значение записываются в чистой области страницы пока не исчерпают свободное место. В этом случае стирается
 * вся страница и запись производится с начала страницы. Данный агоритм применен для увеличения срока службы флеш,
 * так как флеш имеет ограниченное количество стираний - 10000раз.\n
 * Поиск последней записи во флеш производит функция #searchFlash. Считывание найденной записи #loadFlash, запись измененых
 * коэффициентов - #saveFlash.
 *
 */
