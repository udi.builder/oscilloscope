#include "adc.h"

uint8_t lcd[VIEW_WIDTH]; //!< массив значений координат оси Y для построения осцилограммы
volatile uint16_t view_count; //!< величина длительности осцилограммы для перерисовки за один цикл
volatile uint16_t view_start; //!< начальная точка осцилограммы для перерисовки за один цикл

/*!
 * структура для горизонтальной развертки со значениями коэффициента суммирования значений, шагом сетки, размерности значений.
 */
const STORAGE storage[]={
		{1,1,0.84375,1,1," мс/дел"},
		{2,1,0.84375,2,1," мс/дел"},
		{5,1,0.84375,5,1," мс/дел"},
		{10,2,0.84375/2.0,1,0," мс/дел"},
		{20,4,0.84375/4.0,2,0," мс/дел"},
		{50,10,0.84375/10.0,5,0," мс/дел"},
		{100,20,0.84375/20.0,10,0," мс/дел"},
		{200,40,0.84375/40.0,20,0," мс/дел"},
		{500,100,0.84375/100.0,50,0," мс/дел"},
		{1000,200,0.84375/200.0,1,1," с/дел"},
		{2000,400,0.84375/400.0,2,1," с/дел"}};

/*!
 * функция инициализации АЦП1, ПДП1 и таймера 4, для тракта измерения входного сигнала
 * АЦП 1 работает на частоте 12 МГц. Период измерений 28.5 такта = 3.42мкс.
 * Оцифрованные данные скидываются в память с помощью DMA1 channel1.
 * АЦП запускается каждые 4 мкс по совпадению канала 4 таймера 4.
 */
void initADC(void){
	// Таймер 4 запуск ацп каждые 4 мкс.
	RCC->AHBENR |= RCC_AHBPeriph_DMA1;
	RCC->APB1ENR |= RCC_APB1Periph_TIM4;
	RCC->APB2ENR |= RCC_APB2Periph_GPIOB | RCC_APB2Periph_ADC1;

	GPIOB->CRL &= ~(GPIO_CRL_MODE1 | GPIO_CRL_CNF1);

	TIM4->ARR = 287;
	TIM4->CR2 = 0;
	TIM4->CCER = TIM_CCER_CC4E;
	TIM4->CCMR2 =  TIM_CCMR2_OC4M_1 |  TIM_CCMR2_OC4M_2;
	TIM4->CCR4 = 143;

	DMA1_Channel1->CPAR=(uint32_t)&ADC1->DR+1;
	DMA1_Channel1->CMAR=(uint32_t)&measure[0];
	DMA1_Channel1->CNDTR=MEASURING_COUNT*2;
	DMA1_Channel1->CCR = DMA_CCR1_PL_1 | /*DMA_CCR1_MSIZE_0 | DMA_CCR1_PSIZE_0 |*/ DMA_CCR1_MINC;
	DMA1_Channel1->CCR |= DMA_CCR1_CIRC | DMA_CCR1_HTIE | DMA_CCR1_TCIE | DMA_CCR1_EN;

	ADC1->SMPR2 = ADC_SMPR2_SMP0_0 | ADC_SMPR2_SMP0_1; // ADC_SampleTime_28Cycles5
	ADC1->SQR1 = 0;
	ADC1->SQR3 = ADC_SQR3_SQ1_0 | ADC_SQR3_SQ1_3; // ADC_Channel_9
	ADC1->CR1 = 0;
	ADC1->CR2  = ADC_CR2_EXTTRIG | ADC_CR2_EXTSEL_0 | ADC_CR2_EXTSEL_2 | ADC_CR2_DMA | ADC_CR2_RSTCAL | ADC_CR2_ADON ;
	ADC1->CR2 |= ADC_CR2_ALIGN;
	while (ADC1->CR2 & ADC_CR2_RSTCAL);
	ADC1->CR2 |= ADC_CR2_CAL;
	while (ADC1->CR2 & ADC_CR2_CAL);
	TIM4->CR1 = TIM_CR1_CEN;
}
