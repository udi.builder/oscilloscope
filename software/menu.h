/*
 * menu.h
 *
 *  Created on: 04.06.2014
 *      Author: igor
 */

#ifndef MENU_H_
#define MENU_H_
#include <misc.h>
#include "common.h"

enum{e_refreshMain=0,e_refreshSelect,e_refreshResponse,e_refreshMenuTouch,
	e_initMain,e_initSelect,e_initResponse};

extern uint8_t storage_k;
extern uint8_t amplitude_k;

//! инициализация
void initMenu(uint_fast8_t);
//! навигация по меню.
void Navigator(uint_fast8_t key);
//! проверка необходимости запуска калибровки сенсорного экрана
void checkMenuTouch(void);
//! функция возвращает номер актуального меню.
uint8_t getMenu(void);
//! оцифровка оси Y осцилограммы
void setAmplitude(void);
//! оцифровка оси Х осцилограммы
void setStorage(void);
void setBell(void);
#endif /* MENU_H_ */
